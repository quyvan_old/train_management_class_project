A GUI train management system designed for managers, employees, and clients used to coordinate reservations, and manage trains using java.

Co-developed for 60-321 (Advanced Software Techniques). The purpose of this project is to simulate the steps taken to design and develop a large application. We created UML diagrams to plan the classes required for the application. Afterwards it was divided such that I mainly worked on the logic and models, and my partner mainly worked on the UI.

**Core Features:**

* Create, delete, and edit: clients, employees, managers, trains, train stations, reservations 

* Proper permissions to access certain features

* Messaging to proper user if there is a relevant change (example: when a reservation has been cancelled, the client is informed)

**Structure:**

* ./bin - contains binary files

* ./data - contains the data files and miscellaneous files such as the UML diagrams.

* ./src - contains the java files